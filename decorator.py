#Decorator, presentado por christian camilo ventes y juan sebastian vega

class Marine:
    def __init__(self, damage, armor):
        self.damage=damage
        self.armor=armor

class Marine_weapon_upgrade:
    def __init__(self, marine):
        self.damage=marine.damage+1
        self.armor=marine.armor

class Marine_armor_upgrade:
    def __init__(self, marine):
        self.damage=marine.damage
        self.armor=marine.armor+1



print('Basic tests')

print('Single upgrade')

marine = Marine(10, 1)

print('datos del marine: ')
print('daño: ',marine.damage)
print('Armor: ', marine.armor)

print('weapon upgrade: ')
marine = Marine_weapon_upgrade(marine)
print('datos del marine: ')
print('daño: ',marine.damage)
print('Armor: ', marine.armor)

print('armor upgrade: ')
marine = Marine_armor_upgrade(marine)
print('datos del marine: ')
print('daño: ',marine.damage)
print('Armor: ', marine.armor)


        
