#Presentado por christian ventes  y sebastian vega
#Patron strategy

class Movement:
  def move(self,unit):
      pass

class Fly(Movement):
    def move(self,unit):
        unit.position += 10
        
class Walk(Movement):
    def move(self,unit):
        unit.position += 1

class Viking():
    def __init__(self):
        self.position = 0
        self.move_behavior = Walk()
  
    def move(self):
        return self.move_behavior.move(self)

print('Strategy')
viking = Viking()
print('posicion original: ')
print(viking.position)
print('Walk Move')
viking.move()
print('nueva posicion: ')
print(viking.position)
print('Walk Move')
viking.move()
print('nueva posicion: ')
print(viking.position)
print('Fly Move')
viking.move_behavior = Fly()
viking.move()
print('nueva posicion: ')
print(viking.position)
print('Walk Mode')
viking.move_behavior = Walk()
viking.move()
print('nueva posicion: ')
print(viking.position)


    
    


