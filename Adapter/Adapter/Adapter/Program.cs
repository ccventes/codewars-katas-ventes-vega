﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter {
    //Clase adapter presentado por christian ventes y juans ebastian vega
    //Clase que debia hacerse pre -load
    public class Target {
        public int Health { get; set; }
    }
    public interface IUnit {
        void Attack(Target target);
    }

    public class Marine : IUnit {
        public void Attack(Target target) {
            target.Health -= 6;
        }
    }

    public class Zealot : IUnit {
        public void Attack(Target target) {
            target.Health -= 8;
        }
    }

    public class Zergling : IUnit {
        public void Attack(Target target) {
            target.Health -= 5;
        }
    }

    public class Mario {
        public int jumpAttack() {
            Console.WriteLine("Mamamia!");
            return 3;
        }
    }

    //Adicion de clase Mario Adapter

    public class MarioAdapter  {
        private Mario _mario;

        public MarioAdapter(Mario mario) {
            _mario = mario;
        }

        public void Attack(Target target) {
            target.Health -= _mario.jumpAttack();
        }
    }


    //Programa principal
    class Program {
        static void Main(string[] args) {

            //prueba de carga de las clases
            Console.WriteLine("esto es una prueba");
            Mario mario = new Adapter.Mario();
            Console.WriteLine(mario.jumpAttack());

            //Prueba del adapter
            var marioAdapter = new MarioAdapter(new Mario());
            var target = new Target { Health = 33 };

            marioAdapter.Attack(target);
            Console.WriteLine("El nuevo health del target es ");
            Console.WriteLine(target.Health);

            //Assert.AreEqual(30, target.Health);


        }
    }
}
