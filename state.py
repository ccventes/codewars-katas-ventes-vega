#Presentado por christian ventes  y sebastian vega
#Patron state

class SiegeState:
    def __init__(self, mover=False, daño=20):
        self.mover = mover
        self.daño = daño


class TankState:
    def __init__(self, mover=True, daño=5):
        self.mover = mover
        self.daño = daño

class Tank:
    def __init__(self, state = None):
        self.state = TankState()

    def can_move(self):
        return self.state.mover
  
    def damage(self):
        return self.state.daño


print('Basic tests')
print('Tank State')
tank = Tank()
print('Tank can move should be true')
print(tank.can_move())
print('damage should be 5')
print(tank.damage())
print('Siege State')
tank2 = Tank()
tank2.state = SiegeState()
print('Tank can move should be false')
print(tank2.can_move())
print('damage should be 20')
print(tank2.damage())
    
    
    
    
