﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command {
    //Command Presentado por christian Camilo Ventes y Juan Sebastian vega
    //Preload Clases and interfaces
    public interface ICommand {
        void Execute();
        bool CanExecute();
    }

    public interface IUnit {
        int Minerals { get; set; }
        Point Position { get; set; }

        void Move(int x, int y);
        void Gather();
    }

    public class Point {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class Probe : IUnit {
        public Probe() {
            Commands = new Queue<ICommand>();
            Position = new Point { X = 0, Y = 0 };
        }
        public Queue<ICommand> Commands { get; set; }

        public int Minerals { get; set; }

        public Point Position { get; set; }

        public void Move(int x, int y) {
            Commands.Enqueue(new MoveCommand(this, x, y));
        }

        public void Gather() {
            Commands.Enqueue(new GatherCommand(this));
        }
    }
    //Commands
    public class MoveCommand : ICommand {
        private IUnit unit;
        private int x;
        private int y;

        public MoveCommand(IUnit unit, int x, int y) {
            this.unit = unit;
            this.x = x;
            this.y = y;
        }

        public bool CanExecute() {
            return true;
        }

        public void Execute() {
            unit.Position.X = x;
            unit.Position.Y = y;
        }
    }

    public class GatherCommand : ICommand {
        private IUnit unit;

        public GatherCommand(IUnit unit) {
            this.unit = unit;
        }

        public bool CanExecute() {
            return unit.Minerals == 0;
        }

        public void Execute() {
            if (CanExecute()) {
                unit.Minerals += 5;
            }
        }
    }

    class Program {
        static void Main(string[] args) {

            var unit = new Probe();
            var moveCommand = new MoveCommand(unit, 7, 9);

            moveCommand.Execute();

            var expected = new Point { X = 7, Y = 9 };

            Console.WriteLine("Posicion en X real: ");
            Console.WriteLine(unit.Position.X);
            Console.WriteLine("posicion esperada en X: ");
            Console.WriteLine(expected.X);
            Console.WriteLine("Posicion en Y real: ");
            Console.WriteLine(unit.Position.Y);
            Console.WriteLine("posicion esperada en Y: ");
            Console.WriteLine(expected.Y);
        }
    }
}
